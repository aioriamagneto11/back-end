package is.proyecto.backend.data;

import org.springframework.data.repository.CrudRepository;

import is.proyecto.backend.model.Grupo;

public interface GrupoRepository extends CrudRepository<Grupo, Integer> {

}
