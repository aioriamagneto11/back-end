package is.proyecto.backend.data;

import org.springframework.data.repository.CrudRepository;

import is.proyecto.backend.model.Alumno;

public interface AlumnoRepository extends CrudRepository<Alumno, Integer> {
	
	<Optional> Alumno findByMatricula(String matricula);

}
