package is.proyecto.backend.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new post dto.
 *
 * @param id        the id
 * @param contenido the contenido
 * @param fecha     the fecha
 * @param posts     the posts
 */
@AllArgsConstructor

/**
 * Instantiates a new post dto.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
public class PostDto {

	/** The id. */
	private Integer id;

	/** The contenido. */
	private String contenido;

	/** The fecha. */
	private long fecha;

	/** The posts. */
	@Builder.Default
	private final List<Integer> postsQueLoCitan = new ArrayList<>();

}
