package is.proyecto.backend.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new grupo dto.
 *
 * @param groupId     the group id
 * @param descripcion the descripcion
 * @param password    the password
 * @param division    the division
 * @param posts       the posts
 */
@AllArgsConstructor

/**
 * Instantiates a new grupo dto.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
public class GrupoDto {

	/** The group id. */
	private Integer groupId;

	/** The descripcion. */
	private String descripcion;

	/** The password. */
	private String password;

	/** The division. */
	private String division;

	/** The posts. */
	@Builder.Default
	private List<Integer> posts = new ArrayList<>();

}
