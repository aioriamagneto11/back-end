package is.proyecto.backend.dto;

import org.joda.time.DateTime;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import is.proyecto.backend.model.Post;

@Mapper(componentModel = "spring")
public interface PostMapper {

	PostMapper INSTANCE = Mappers.getMapper(PostMapper.class);

	PostDto toDto(Post post);

	Post fromDto(PostDto dto);

	default Integer asInteger(Post post) {
		return post.getId();
	}

	Post fromInteger(Integer id);
	
	default DateTime map(long value) {
		return new DateTime(value);
	}
	
	default long map(DateTime value) {
		return value.getMillis();
	}

}
