package is.proyecto.backend.dto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import is.proyecto.backend.model.Division;

@Mapper(componentModel = "spring")
public interface DivisionMapper {

	DivisionMapper INSTANCE = Mappers.getMapper(DivisionMapper.class);

	DivisionDto toDto(Division division);

	Division fromDto(DivisionDto dto);

	default String asString(Division division) {
		return division.getSiglas();
	}

	Division fromString(String siglas);

}
