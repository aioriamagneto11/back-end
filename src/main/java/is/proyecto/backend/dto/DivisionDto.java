package is.proyecto.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new division dto.
 *
 * @param siglas the siglas
 * @param nombre the nombre
 */
@AllArgsConstructor

/**
 * Instantiates a new division dto.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
public class DivisionDto {

	/** The siglas. */
	private String siglas;

	/** The nombre. */
	private String nombre;

}
