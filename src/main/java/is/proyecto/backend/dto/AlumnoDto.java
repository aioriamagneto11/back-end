package is.proyecto.backend.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new alumno dto.
 *
 * @param id        the id
 * @param correo    the correo
 * @param matricula the matricula
 * @param password  the password
 * @param division  the division
 * @param posts     the posts
 * @param groups    the groups
 */
@AllArgsConstructor

/**
 * Instantiates a new alumno dto.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
public class AlumnoDto {

	/** The correo. */
	private String correo;

	/** The matricula. */
	private String matricula;
	
	/** The token */
	private String token;
	
	/** The division. */
	private String division;

	/** The posts. */
	@Builder.Default
	private final List<Integer> posts = new ArrayList<>();

	/** The groups. */
	@Builder.Default
	private final List<Integer> groups = new ArrayList<>();

}
