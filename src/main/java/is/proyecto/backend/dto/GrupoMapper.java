package is.proyecto.backend.dto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import is.proyecto.backend.model.Grupo;

@Mapper(componentModel = "spring", uses = { DivisionMapper.class, PostMapper.class })
public interface GrupoMapper {

	GrupoMapper INSTANCE = Mappers.getMapper(GrupoMapper.class);

	GrupoDto toDto(Grupo grupo);

	Grupo fromDto(GrupoDto dto);

	default Integer asInteger(Grupo grupo) {
		return grupo.getGroupId();
	}

	Grupo fromInteger(Integer id);

}
