package is.proyecto.backend.dto;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class ListDto.
 */
public class ListDto {

	/** The list. */
	private List<?> list;

	/**
	 * Instantiates a new list dto.
	 *
	 * @param list the list
	 */
	public ListDto(List<?> list) {
		this.list = list;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<?> getList() {
		return list;
	}

}
