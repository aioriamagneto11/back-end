package is.proyecto.backend.service;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import is.proyecto.backend.data.DivisionRepository;
import is.proyecto.backend.dto.DivisionDto;
import is.proyecto.backend.dto.DivisionMapper;
import is.proyecto.backend.model.Division;
import lombok.extern.slf4j.Slf4j;

/** The Constant log. */
@Slf4j
@Service
public class DivisionService {

	/** The division repository. */
	@Autowired
	private DivisionRepository divisionRepository;

	/** The division mapper. */
	@Autowired
	private DivisionMapper divisionMapper;

	/**
	 * Creates the.
	 *
	 * @param divisionDto the division dto
	 * @return the division dto
	 */
	@Transactional
	public DivisionDto create(DivisionDto divisionDto) {

		Division division = divisionMapper.fromDto(divisionDto);

		division = divisionRepository.save(division);

		divisionDto.setSiglas(division.getSiglas());

		return divisionDto;
	}

	/**
	 * Find all.
	 *
	 * @return the iterable
	 */
	public Iterable<DivisionDto> findAll() {

		Iterable<Division> resultsIterable = divisionRepository.findAll();

		List<DivisionDto> results = new ArrayList<>();

		for (Division division : resultsIterable) {
			DivisionDto divisionDto = divisionMapper.toDto(division);

			results.add(divisionDto);

		}

		return results;
	}

	/**
	 * Find.
	 *
	 * @param id the id
	 * @return the optional
	 */
	public Optional<DivisionDto> find(String siglas) {

		Optional<Division> optDivision = divisionRepository.findById(siglas);

		DivisionDto divisionDto = null;

		if (optDivision.isPresent()) {
			Division division = optDivision.get();

			divisionDto = divisionMapper.toDto(division);
		}

		return Optional.ofNullable(divisionDto);
	}

	/**
	 * Update.
	 *
	 * @param id      the id
	 * @param updates the updates
	 * @return the division dto
	 */
	@Transactional
	public DivisionDto update(String siglas, Map<String, Object> updates) {

		Optional<Division> optDivision = divisionRepository.findById(siglas);

		if (optDivision.isEmpty()) {
			return null;
		}

		Division division = optDivision.get();

		for (String key : updates.keySet()) {
			try {

				if (!key.equals("siglas")) {
					Method setter = new PropertyDescriptor(key, Division.class).getWriteMethod();

					setter.invoke(division, updates.get(key));
				}
			} catch (Exception ex) {
				log.info("Could not set key " + key + " value: " + updates.get(key) + " type "
						+ updates.get(key).getClass() + " exception:" + ex.getMessage());
			}
		}

		division = divisionRepository.save(division);

		return divisionMapper.toDto(division);

	}

	/**
	 * Delete.
	 *
	 * @param id the id of the division to delete
	 * @return true, if successful, false if category not found in database
	 */
	@Transactional
	public boolean delete(String siglas) {

		Optional<Division> optDivision = divisionRepository.findById(siglas);

		if (optDivision.isEmpty()) {
			return false;
		}

		divisionRepository.delete(optDivision.get());

		return true;

	}

}
