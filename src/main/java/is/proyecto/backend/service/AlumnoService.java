package is.proyecto.backend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import is.proyecto.backend.data.AlumnoRepository;
import is.proyecto.backend.data.DivisionRepository;
import is.proyecto.backend.dto.AlumnoDto;
import is.proyecto.backend.dto.AlumnoMapper;
import is.proyecto.backend.model.Alumno;
import is.proyecto.backend.model.Division;

/**
 * Clase AlumnoService que valida las reglas de negocio.
 * 
 * @author Fabián
 *
 */
@Service
public class AlumnoService {

	/** The grupo repository. */
	@Autowired
	private AlumnoRepository alumnoRepository;
	
	@Autowired
	private DivisionRepository divisionRepository;

	/** The grupo mapper. */
	@Autowired
	private AlumnoMapper alumnoMapper;
	
	/**
	 * Creates the.
	 *
	 * @param alumnoDto the alumno dto
	 * @return the alumno dto
	 */
	@Transactional
	public AlumnoDto create(AlumnoDto alumnoDto) {
		
		Division division = divisionRepository.findById(alumnoDto.getDivision()).get();
		
		Alumno alumno = Alumno.builder().correo(alumnoDto.getCorreo()).matricula(alumnoDto.getMatricula()).division(division).password(alumnoDto.getToken()).build();
		
		alumno.setPassword(alumnoDto.getToken());
		
		alumno = alumnoRepository.save(alumno);
		
		alumnoDto = alumnoMapper.toDto(alumno);
		
		alumnoDto.setToken("");
		
		return alumnoDto;
	}
	
	public Iterable<AlumnoDto> findAll() {
		Iterable<Alumno> resultsIterable = alumnoRepository.findAll();
		List<AlumnoDto> results = new ArrayList<>();
		for (Alumno alumno : resultsIterable) {
			AlumnoDto alumnoDto = alumnoMapper.toDto(alumno);

			results.add(alumnoDto);

		}

		return results;
	}

	public Optional<AlumnoDto> findById(Integer id) {
		
		Optional<Alumno> optAlumno = alumnoRepository.findById(id);

		AlumnoDto alumnoDto = null;

		if (optAlumno.isPresent()) {
			Alumno alumno = optAlumno.get();
			alumnoDto = alumnoMapper.toDto(alumno);
		}

		return Optional.ofNullable(alumnoDto);
	}
	
	public Optional<AlumnoDto> findByMatricula(String matricula){
		
		Optional<Alumno> optAlumno = Optional.ofNullable(alumnoRepository.findByMatricula(matricula));

		AlumnoDto alumnoDto = null;

		if (optAlumno.isPresent()) {
			Alumno alumno = optAlumno.get();
			alumnoDto = alumnoMapper.toDto(alumno);
		}

		return Optional.ofNullable(alumnoDto);
	}
	
	public boolean validaPasswordAlumno(String matricula, String password) {
		
		Optional<Alumno> optAlumno = Optional.ofNullable(alumnoRepository.findByMatricula(matricula));

		Alumno alumno;

		if (optAlumno.isPresent()) {
			alumno = optAlumno.get();
			
			if (alumno.getPassword().equals(password))
				return true;
			else
				return false;			
			
		} else {
			return false;
		}
	}

}
