package is.proyecto.backend.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new division.
 *
 * @param siglas the siglas
 * @param nombre the nombre
 */
@AllArgsConstructor

/**
 * Instantiates a new division.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
@Entity
public class Division {

	/** The siglas. */
	@Id
	private String siglas;

	/** The nombre. */
	private String nombre;

}
