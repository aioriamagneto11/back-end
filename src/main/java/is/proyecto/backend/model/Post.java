package is.proyecto.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.jadira.usertype.dateandtime.joda.PersistentDateTime;
import org.joda.time.DateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Instantiates a new post.
 *
 * @param id        the id
 * @param contenido the contenido
 * @param fecha     the fecha
 */
@AllArgsConstructor

/**
 * Instantiates a new post.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
@Entity
@TypeDef(name = "postDateTime", typeClass = PersistentDateTime.class)
public class Post {

	/** The id. */
	@Id
	@GeneratedValue
	private Integer id;

	/** The contenido. */
	@Column(nullable = false)
	private String contenido;

	/** The fecha. */
	@Columns(columns = { @Column(name = "fecha_post") })
	@Type(type = "postDateTime")
	private DateTime fecha;

	/** The posts that quote the post. */
	@OneToMany(targetEntity = Post.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, orphanRemoval = true)
	private final List<Post> postsQueLoCitan = new ArrayList<>();

	public void addQuotingPost(Post quotingPost) {
		this.postsQueLoCitan.add(quotingPost);
	}

}
