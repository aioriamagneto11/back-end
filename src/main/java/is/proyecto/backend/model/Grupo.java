package is.proyecto.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new grupo.
 *
 * @param groupId     the group id
 * @param descripcion the descripcion
 * @param password    the password
 * @param division    the division
 */
@AllArgsConstructor

/**
 * Instantiates a new grupo.
 */
@NoArgsConstructor

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Builder
@Entity
public class Grupo {

	/** The group id. */
	@Id
	@GeneratedValue
	private Integer groupId;

	/** The descripcion. */
	@Column(nullable = false)
	private String descripcion;

	/** The password. */
	private String password;

	/** The division. */
	@ManyToOne
	private Division division;

	/** The posts. */
	@OneToMany(targetEntity = Post.class, fetch = FetchType.LAZY, cascade = CascadeType.MERGE, orphanRemoval = true)
	private final List<Post> posts = new ArrayList<>();

	public boolean addPost(Post post) {
		return posts.add(post);
	}

}
