package is.proyecto.backend.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import is.proyecto.backend.dto.AlumnoDto;
import is.proyecto.backend.service.AlumnoService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "*")
@Api(value = "login")
public class LoginController {
	
	@Autowired
	AlumnoService alumnoService;

	@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> login(@RequestParam("user") String matricula, @RequestParam("password") String pass) {
		
		AlumnoDto alumnoDto;
		
		Optional <AlumnoDto> optAlumnoDto = alumnoService.findByMatricula(matricula);
		
		if (optAlumnoDto.isPresent()) {
			alumnoDto = optAlumnoDto.get();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Usuario o contraseña incorrectos");
		}
		
		if (!alumnoService.validaPasswordAlumno(matricula, pass)) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Usuario o contraseña incorrectos");
		}
		
		String token = getJWTToken(matricula);
		
		alumnoDto.setToken(token);
		
		return ResponseEntity.status(HttpStatus.OK).body(alumnoDto);
				
	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("fabianeslaonda")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
}