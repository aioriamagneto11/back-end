package is.proyecto.backend.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import is.proyecto.backend.dto.AlumnoDto;
import is.proyecto.backend.model.Alumno;
import is.proyecto.backend.service.AlumnoService;

/**
 * La clase AlumnoController con los endpoints necesarios
 * 
 * @author Fabián
 *
 */

@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "*")
@Api(value = "alumno")
public class AlumnoController {

	/** The Alumno service. */
	@Autowired
	private AlumnoService alumnoService;
	
	/**
	 * Creates a alumno
	 *
	 * @param alumnoDto the alumno dto to create
	 * @return the response entity created
	 */
	@ApiOperation(value = "Crea un nuevo alumno", notes = "Permite crear un nuevo alumno, no se pueden crear alumnos con la misma matrícula")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Alumno creado exitosamente", response = AlumnoDto.class),
			@ApiResponse(code = 400, message = "Alumno inválido", response = AlumnoDto.class),
			@ApiResponse(code = 500, message = "Error en el servidor al crear alumno") })
	@PostMapping(path = "/alumnos", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AlumnoDto> create(
			@RequestBody @Valid @ApiParam(name = "alumno", value = "Alumno a ser creado", required = true) AlumnoDto alumnoDto) {

		try {
			alumnoDto = alumnoService.create(alumnoDto);
			
		} catch (Exception e) {
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		
		if (alumnoDto == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(alumnoDto);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(alumnoDto);
	}

	/**
	 * 
	 * @param matricula La matrícula del alumno
	 * @return Regresa el alumno
	 */
	@ApiOperation(value = "Obtiene un alumno usando su matrícula", notes = "Obtiene un alumno según su matrícula, ésta es única")
	@GetMapping(path = "/alumnos/{matricula}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Alumno encontrado", response = Alumno.class),
			@ApiResponse(code = 404, message = "Alumno no encontrado", response = String.class) })
	public ResponseEntity<?> retrieveById(
			@ApiParam(name = "matricula", value = "Matricula del alumno", required = true) @PathVariable(name = "matricula", required = true) String matricula) {
		
		Optional<AlumnoDto> optAlumnoDto = alumnoService.findByMatricula(matricula);

		if (optAlumnoDto.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(optAlumnoDto.get());
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Alumno no encontrado");
		}
	}

}
