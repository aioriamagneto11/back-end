package is.proyecto.backend.controller;

import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import is.proyecto.backend.dto.DivisionDto;
import is.proyecto.backend.service.DivisionService;

/**
 * The Class DivisionController.
 */
@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "*")
@Api(value = "division")
public class DivisionController {

	/** The division service. */
	@Autowired
	private DivisionService divisionService;

	/**
	 * Creates a division
	 *
	 * @param divisionDto the division dto to create
	 * @return the response entity created
	 */
	@ApiOperation(value = "Crea una nueva división", notes = "Permite crear una nueva división, las siglas de las divisiones son únicas")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "División creada exitosamente", response = DivisionDto.class),
			@ApiResponse(code = 400, message = "División inválida", response = DivisionDto.class),
			@ApiResponse(code = 500, message = "Error en el servidor al crear división", response = String.class) })
	@PostMapping(path = "/divisiones", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DivisionDto> create(
			@RequestBody @Valid @ApiParam(name = "division", value = "División a ser creada", required = true) DivisionDto divisionDto) {

		try {
			divisionDto = divisionService.create(divisionDto);
		} catch (Exception e) {
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		if (divisionDto == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(divisionDto);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(divisionDto);
	}

	/**
	 * Retrieve all divisions.
	 *
	 * @return the response entity with all the divisions
	 */
	@ApiOperation(value = "Obtiene todas las divisiones", notes = "Obtiene una lista de todas las divisiones actualmente en el sistema")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Divisiones obtenidas", response = DivisionDto.class, responseContainer = "List") })
	@GetMapping(path = "/divisiones", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> retrieve() {

		Iterable<DivisionDto> results = divisionService.findAll();

		return ResponseEntity.status(HttpStatus.OK).body(results);

	}

	/**
	 * Retrieve a division by its acronym
	 *
	 * @param siglas the siglas
	 * @return the response entity if found
	 */
	@ApiOperation(value = "Obtiene una división por sus siglas", notes = "Obtiene una división según sus siglas")
	@GetMapping(path = "/divisiones/{siglasDivision}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "División encontrada", response = DivisionDto.class),
			@ApiResponse(code = 404, message = "División no encontrada", response = String.class) })
	public ResponseEntity<Object> retrieve(
			@ApiParam(name = "siglasDivision", value = "Siglas de la división", required = true) @PathVariable(name = "siglasDivision", required = true) String siglas) {

		Optional<DivisionDto> optDivisionDto = divisionService.find(siglas);

		if (optDivisionDto.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id " + siglas + " not found");
		}

		return ResponseEntity.status(HttpStatus.OK).body(optDivisionDto.get());

	}

	/**
	 * Patch a division
	 *
	 * @param updates the updates to perform over the division
	 * @param siglas  the siglas of the division
	 * @return the response entity if it exists
	 */
	@ApiOperation(value = "actualiza valores de una división", notes = "Actualiza los valores de una división (a excepción de sus siglas)")
	@PatchMapping(path = "/divisiones/{siglasDivision}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "División actualizada exitosamente", response = DivisionDto.class),
			@ApiResponse(code = 400, message = "División no encontrada", response = String.class),
			@ApiResponse(code = 409, message = "La división no puede ser actualizada en este momento", response = String.class) })
	public ResponseEntity<Object> patch(@RequestBody Map<String, Object> updates,
			@ApiParam(name = "siglasDivision", value = "Siglas de la división", required = true) @PathVariable(name = "siglasDivision", required = true) String siglas) {

		DivisionDto divisionDto = null;

		try {
			divisionDto = divisionService.update(siglas, updates);

			if (divisionDto == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id " + siglas + " not found");
			}

		} catch (Exception ex) {
			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

		return ResponseEntity.status(HttpStatus.OK).body(divisionDto);

	}

	/**
	 * Delete.
	 *
	 * @param siglas the siglas of the division
	 * @return status according if deleted sucessfully or not
	 */
	@ApiOperation(value = "borra una división", notes = "Borra una división según sus siglas")
	@DeleteMapping(path = "/divisiones/{siglasDivision}")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "División borrada exitosamente"),
			@ApiResponse(code = 400, message = "División no encontrada", response = String.class),
			@ApiResponse(code = 409, message = "La división no puede ser borrada en este momento", response = String.class) })
	public ResponseEntity<Object> delete(
			@ApiParam(name = "siglasDivision", value = "Siglas de la división", required = true) @PathVariable(name = "siglasDivision", required = true) String siglas) {

		boolean retval;

		try {
			retval = divisionService.delete(siglas);

			if (!retval) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id " + siglas + " not found");
			}

		} catch (Exception ex) {

			throw new HttpClientErrorException(HttpStatus.CONFLICT, ex.getMessage());
		}

		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

	}

}
