package is.proyecto.backend;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import is.proyecto.backend.data.AlumnoRepository;
import is.proyecto.backend.data.DivisionRepository;
import is.proyecto.backend.data.GrupoRepository;
import is.proyecto.backend.data.PostRepository;
import is.proyecto.backend.dto.GrupoMapper;
import is.proyecto.backend.dto.PostMapper;
import is.proyecto.backend.model.Alumno;
import is.proyecto.backend.model.Division;
import is.proyecto.backend.model.Grupo;
import is.proyecto.backend.model.Post;
import is.proyecto.backend.service.GrupoService;
import is.proyecto.backend.service.PostService;

@Component
public class DataInitializer implements InitializingBean {

	@Autowired
	PostRepository postRepository;

	@Autowired
	PostService postService;

	@Autowired
	DivisionRepository divisionRepository;

	@Autowired
	AlumnoRepository alumnoRepository;

	@Autowired
	GrupoRepository grupoRepository;

	@Autowired
	GrupoService grupoService;
	
	@Autowired
	GrupoMapper grupoMapper;
	
	@Autowired
	PostMapper postMapper;

	@Override
	public void afterPropertiesSet() throws Exception {

		Division cbi = Division.builder().siglas("CBI").nombre("División de ciencias básicas e ingeniería").build();
		divisionRepository.save(cbi);
		Division cbs = Division.builder().siglas("CBS").nombre("División de ciencias biológicas y de la salud").build();
		divisionRepository.save(cbs);

		Grupo grupo1 = Grupo.builder().descripcion("Grupo de estudio para CBI").password("cbi").division(cbi).build();
		grupo1 = grupoRepository.save(grupo1);

		Grupo grupo2 = Grupo.builder().descripcion("Grupo de estudio para CBS").division(cbs).build();
		grupo2 = grupoRepository.save(grupo2);

		Alumno fabian = Alumno.builder().matricula("2163008591").correo("cbi2163008591@titlani.uam.mx")
				.password("33f1ac9818eb1eb88c0156eada16bae1fc00fb98e89645aab3c084a86f66bc63").division(cbi).build();

		Alumno daniel = Alumno.builder().matricula("2163001919").correo("cbi2163001919@titlani.uam.mx")
				.password("bd3dae5fb91f88a4f0978222dfd58f59a124257cb081486387cbae9df11fb879").division(cbs).build();

		Post post1 = Post.builder().contenido("Primer post").fecha(new DateTime(Instant.now())).build();
		post1 = postRepository.save(post1);

		Post post2 = Post.builder().contenido("Segundo post").fecha(new DateTime(Instant.now())).build();
		post2 = postRepository.save(post2);

		Post post3 = Post.builder().contenido("Tercer post").fecha(new DateTime(Instant.now())).build();
		post3 = postRepository.save(post3);

		postService.addQuotingPostToPost(post1.getId(), post2.getId());

		grupoService.addPostToGroup(grupoMapper.toDto(grupo1), postMapper.toDto(post1));
		grupoService.addPostToGroup(grupoMapper.toDto(grupo1), postMapper.toDto(post2));
		grupoService.addPostToGroup(grupoMapper.toDto(grupo2), postMapper.toDto(post3));

		fabian.getGroups().add(grupo1);
		fabian.getPosts().add(post1);
		fabian.getPosts().add(post2);
		fabian = alumnoRepository.save(fabian);

		daniel.getGroups().add(grupo2);
		daniel.getPosts().add(post3);
		daniel = alumnoRepository.save(daniel);

	}

}
